package com.example.myloancalculator_androidlab2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import UI.*;
import abcbank.LoanCalculator
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.round

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Clears the input fields
     */
    fun clearEventHandler(view: View) {
        loanAmountInput?.setText("");
        termOfLoanInput?.setText("");
        yearlyInterestInput?.setText("");
        monthlyPayment.text = "";
        totalPayment.text = "";
        totalInterest.text = "";
        loanAmountInput.requestFocus();
    }

    /**
     * Performs calculations and displays results
     */
    fun calculateEventHandler(view: View) {
        //Get input
        val loanAmountTxt = loanAmountInput.text
        val termOfLoanTxt = termOfLoanInput.text
        val yearlyInterestTxt = yearlyInterestInput.text

        //Make sure everything is there
        if (loanAmountTxt.isBlank() || termOfLoanTxt.isBlank() || yearlyInterestTxt.isBlank()) {
            Toast.makeText(applicationContext, "Need to enter numbers in all fields!", Toast.LENGTH_SHORT).show()
            return
        }

        //Perform calculation
        val loanAmount =  loanAmountTxt.toString().toDouble()
        val termOfLoan = termOfLoanTxt.toString().toInt()
        val yearlyInterest = yearlyInterestTxt.toString().toDouble()

        val calc = LoanCalculator(loanAmount, termOfLoan, yearlyInterest);

        //Display result
        monthlyPayment.text = calc.monthlyPayment.toString();
        totalPayment.text = calc.totalCostOfLoan.toString();
        totalInterest.text = calc.totalInterest.round(2).toString();

    }

    /**
     * Simple extension method to round numbers
     */
    fun Double.round(decimals: Int): Double {
        var multiplier = 1.0;
        repeat(decimals) { multiplier *= 10 }
        return round(this * multiplier) / multiplier;
    }
}
