package abcbank
//   This code was converted using try.kotlinlang.org conversion tool
//   it has not been tested, nor cleaned

class LoanCalculator {
  var loanAmount:Double = 0.0
  var numberOfYears:Int = 0
  var yearlyInterestRate:Double = 0.0
  var monthlyPayment:Double = 0.0
  
  //calculate the monthly payment
  
  get() {
    val monthlyPayment:Double
    val monthlyInterestRate:Double
    val numberOfPayments:Int
    if (numberOfYears != 0 && yearlyInterestRate != 0.0)
    {
      monthlyInterestRate = yearlyInterestRate / 1200
      numberOfPayments = numberOfYears * 12
      monthlyPayment = Math.round(
        ((loanAmount * monthlyInterestRate) / (1 - (1 / Math.pow((1 + monthlyInterestRate), numberOfPayments.toDouble())))
                ) * 100) / 100.0 //Round to 2 decimals
    }
    else
    monthlyPayment = 0.0
    return monthlyPayment
  }
  val totalCostOfLoan:Double
  get() {
    return monthlyPayment * numberOfYears.toDouble() * 12.0
  }
  val totalInterest:Double
  get() {
    return totalCostOfLoan - loanAmount
  }


  constructor() {}
  constructor(loanAmount:Double, numberOfYears:Int,
              yearlyInterestRate:Double) {
    this.loanAmount = loanAmount
    this.numberOfYears = numberOfYears
    this.yearlyInterestRate = yearlyInterestRate
  }

  public override fun toString():String {
    return (loanAmount.toString() + "," + numberOfYears.toString() + "," +
            yearlyInterestRate.toString())
  }
}